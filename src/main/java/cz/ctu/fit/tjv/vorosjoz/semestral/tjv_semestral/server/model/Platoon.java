package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;
import java.util.Objects;

@Entity
@Table
public class Platoon {

    @Id
    @GeneratedValue ( strategy = GenerationType.AUTO )
    private int id;
    @Column ( length = 32, nullable = false, unique = false )
    private String name;
    @Column ( length = 32, nullable = true, unique = false )
    private String nickname;

    private int idArmy;
    @ManyToOne
    private Army army;

    public int getIdArmy() {
        return idArmy;
    }

    public void setIdArmy(int idArmy) {
        this.idArmy = idArmy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Platoon platoon = (Platoon) o;
        return getId() == platoon.getId() && getIdArmy() == platoon.getIdArmy() && Objects.equals(getName(), platoon.getName()) && Objects.equals(getNickname(), platoon.getNickname()) && Objects.equals(getMissions(), platoon.getMissions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getNickname(), getIdArmy(), getMissions());
    }

    @ManyToMany
    private Collection<Mission> missions;

    public Collection<Mission> getMissions() {
        return missions;
    }

    public void setMissions(Collection<Mission> missions) {
        this.missions = missions;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
