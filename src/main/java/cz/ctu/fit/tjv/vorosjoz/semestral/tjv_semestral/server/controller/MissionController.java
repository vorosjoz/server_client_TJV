package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.controller;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Mission;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service.MissionService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/missions")
public class MissionController {


    private MissionService missionService;

    public MissionController( MissionService service) {
        missionService = service;
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation( summary = "All Missions" )
    public Collection<Mission> readAll() {
        return missionService.readAll();
    }

    @Operation( summary = "Mission")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    public Mission read(@PathVariable Integer id) {
        Optional<Mission> optionalEntity = readById(id);
        if (optionalEntity.isPresent()) {
            return optionalEntity.get();
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }

    @Operation( summary = "Create new Mission")
    @ResponseStatus(HttpStatus.OK)
    @PostMapping
    public ResponseEntity<Mission> create(@RequestBody Mission entity) {
        Mission mission = createEntity(entity);
        return new ResponseEntity<>(mission, HttpStatus.CREATED);
    }
    @Operation( summary = "Edit Mission")
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ResponseEntity<Mission> update(@PathVariable Integer id, @RequestBody Mission updatedEntity) {
        Optional<Mission> existingEntity = readById(id);
        if (existingEntity.isPresent()) {
            Mission mission = updateEntity(id, updatedEntity);
            return new ResponseEntity<>(mission, HttpStatus.OK);
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }
    @Operation( summary = "Delete Mission")
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        if (readById(id).isPresent()) {
            deleteEntity(id);
            return ResponseEntity.noContent().build();
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }

    public Optional<Mission> readById(Integer id)
    {
        return missionService.readById(id);
    }

    public Mission createEntity(Mission entity)
    {
        return missionService.createEntity(entity);
    }

    public Mission updateEntity(Integer id, Mission updatedEntity)
    {
        return missionService.updateEntity(id, updatedEntity);
    }

    public void deleteEntity(Integer id)
    {
        missionService.deleteEntity(id);
    }

}