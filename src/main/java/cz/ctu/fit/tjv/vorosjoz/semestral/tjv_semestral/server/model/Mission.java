package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
@Entity
@Table
public class Mission {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column( length = 32, nullable = false, unique = true )
    private String codename;
    @ManyToMany
    private Collection<Platoon> platoons;

    public Collection<Platoon> getPlatoons() {
        return platoons;
    }

    public void setPlatoons(Collection<Platoon> platoons) {
        this.platoons = platoons;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mission mission = (Mission) o;
        return getId() == mission.getId() && Objects.equals(getCodename(), mission.getCodename());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCodename());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }
}
