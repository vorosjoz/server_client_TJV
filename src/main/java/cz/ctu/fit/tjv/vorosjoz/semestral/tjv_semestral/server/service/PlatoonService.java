package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.controller.CustomException;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Platoon;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.repository.PlatoonRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
@Component
public class PlatoonService {

    private PlatoonRepository repository;


    public PlatoonService(PlatoonRepository repository) {
        this.repository = repository;
    }

    public Optional<Platoon> readById(Integer id) {
        return repository.findById(id);
    }

    public Platoon createPlatoon(Platoon entity) {
        return repository.save(entity);
    }

    public void deleteEntity(Integer id) {
        //throw an exception if the entity does not exist
        if (readById(id).isEmpty()) {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
        repository.deleteById(id);
    }

    public Collection<Platoon> readAll() {
        return (Collection<Platoon>) repository.findAll();
    }

    public Platoon updateEntity(Integer id, Platoon updatedPlatoon) {
        Optional<Platoon> optionalExistingPlatoon = repository.findById(id);
        if (optionalExistingPlatoon.isPresent()) {
            Platoon existingPlatoon = optionalExistingPlatoon.get();
            existingPlatoon.setName(updatedPlatoon.getName());
            existingPlatoon.setNickname(updatedPlatoon.getNickname());
            return repository.save(existingPlatoon);
        } else {
            throw new EntityNotFoundException("Platoon not found...");
        }
    }
    public ArrayList<Platoon> findPlatoonsByMissionId (int id )
    {
        return (ArrayList<Platoon>) this.repository.findPlatoonsByMissionId(id);
    }
}
