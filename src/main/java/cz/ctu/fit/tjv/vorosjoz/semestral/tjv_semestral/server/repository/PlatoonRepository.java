package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.repository;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Platoon;
import jakarta.persistence.Id;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlatoonRepository extends CrudRepository<Platoon, Integer > {
    @Query("SELECT DISTINCT p FROM Platoon p JOIN p.missions m WHERE m.id = :missionId")
    List<Platoon> findPlatoonsByMissionId(@Param("missionId") Integer missionId);
}
