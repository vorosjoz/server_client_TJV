package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.controller;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Platoon;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service.PlatoonService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/platoons")
public class PlatoonController {
    private PlatoonService platoonService;
    public PlatoonController( PlatoonService platoonService ) {
        this.platoonService = platoonService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation( summary = "All Platoons" )
    public Collection<Platoon> readAll() {
        return platoonService.readAll();
    }

    @Operation( summary = "Platoon")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    public Platoon read(@PathVariable Integer id) {
        Optional<Platoon> optionalEntity = readById(id);
        if (optionalEntity.isPresent()) {
            return optionalEntity.get();
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }

    @Operation( summary = "Edit Platoon")
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ResponseEntity<Platoon> update(@PathVariable Integer id, @RequestBody Platoon updatedEntity) {
        Optional<Platoon> existingEntity = readById(id);
        if (existingEntity.isPresent()) {
            Platoon platoon = updateEntity(id, updatedEntity);
            return new ResponseEntity<>(platoon, HttpStatus.OK);
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }
    @Operation( summary = "Delete Platoon")
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        if (readById(id).isPresent()) {
            deleteEntity(id);
            return ResponseEntity.noContent().build();
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }

    @Operation( summary = "Find Platoons by Mission ID")
    @GetMapping("/mission/{id}")
    public Collection<Platoon> findByMissionId(@PathVariable Integer id) {
        return platoonService.findPlatoonsByMissionId(id);
    }

    public Optional<Platoon> readById(Integer id)
    {
        return platoonService.readById(id);
    }

    public Platoon createEntity(Platoon entity)
    {
        return platoonService.createPlatoon(entity);
    }

    public Platoon updateEntity(Integer id, Platoon updatedEntity)
    {
        return platoonService.updateEntity(id, updatedEntity);
    }

    public void deleteEntity(Integer id)
    {
        platoonService.deleteEntity(id);
    }

}
