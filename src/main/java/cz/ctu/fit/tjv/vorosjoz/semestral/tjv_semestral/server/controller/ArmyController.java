package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.controller;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Army;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Platoon;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service.ArmyService;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service.PlatoonService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/armies")
public class ArmyController {

    private ArmyService armyService;
    private PlatoonService platoonService;

    public ArmyController(ArmyService armyService, PlatoonService platoonService) {
        this.armyService = armyService;
        this.platoonService = platoonService;
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation( summary = "All Armys" )
    public Collection<Army> readAll() {
        return armyService.readAll();
    }

    @Operation( summary = "Army")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{id}")
    public Army read(@PathVariable Integer id) {
        Optional<Army> optionalEntity = readById(id);
        if (optionalEntity.isPresent()) {
            return optionalEntity.get();
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }

    @Operation( summary = "Create new Army")
    @ResponseStatus(HttpStatus.OK)
    @PostMapping
    public ResponseEntity<Army> create(@RequestBody Army entity) {
        Army army = createEntity(entity);
        return new ResponseEntity<>(army, HttpStatus.CREATED);
    }
    @Operation( summary = "Edit Army")
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ResponseEntity<Army> update(@PathVariable Integer id, @RequestBody Army updatedEntity) {
        Optional<Army> existingEntity = readById(id);
        if (existingEntity.isPresent()) {
            Army army = updateEntity(id, updatedEntity);
            return new ResponseEntity<>(army, HttpStatus.OK);
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Create new Platoon in an Army")
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/{id}/platoons")
    public ResponseEntity<Platoon> createPlatoon(@PathVariable Integer id, @RequestBody Platoon platoon) {
        Optional<Army> existingArmy = armyService.readById(id);
        if (existingArmy.isPresent()) {
            platoon.setIdArmy(id);
            platoon = platoonService.createPlatoon(platoon);
            existingArmy.get().getPlatoons().add(platoon);
            armyService.updateEntity(id, existingArmy.get());
            return new ResponseEntity<>(platoon, HttpStatus.CREATED);
        } else {
            throw new CustomException("Army not found", HttpStatus.NOT_FOUND);
        }
    }

    @Operation( summary = "Delete Army")
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        Optional<Army> existingArmy = readById(id);
        if (existingArmy.isPresent()) {
            Army army = existingArmy.get();
            Collection<Platoon> platoons = new ArrayList<>(army.getPlatoons());
            army.getPlatoons().clear();
            armyService.createEntity(army);
            for (Platoon platoon : platoons) {
                platoonService.deleteEntity(platoon.getId());
            }
            deleteEntity(id);
            return ResponseEntity.noContent().build();
        } else {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
    }

    public Optional<Army> readById(Integer id)
    {
        return armyService.readById(id);
    }

    public Army createEntity(Army entity)
    {
        return armyService.createEntity(entity);
    }

    public Army updateEntity(Integer id, Army updatedEntity)
    {
        return armyService.updateEntity(id, updatedEntity);
    }

    public void deleteEntity(Integer id)
    {
        armyService.deleteEntity(id);
    }

}
