package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.repository;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Mission;
import org.springframework.data.repository.CrudRepository;


public interface MissionRepository extends CrudRepository<Mission, Integer > {

}
