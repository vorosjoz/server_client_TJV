package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TjvSemestralApplication {

    public static void main(String[] args) {

        SpringApplication.run(TjvSemestralApplication.class, args);
        System.out.println("Hello world!");
    }

}



