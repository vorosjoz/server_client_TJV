package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table
public class Army {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column( length = 32, nullable = false, unique = false )
    private String name;

    @OneToMany
    private Collection<Platoon> platoons;

    public Collection<Platoon> getPlatoons() {
        return platoons;
    }

    public void setPlatoons(Collection<Platoon> platoons) {
        this.platoons = platoons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return getId() == army.getId() && Objects.equals(getName(), army.getName()) &&
                Objects.equals(getPlatoons(), army.getPlatoons());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPlatoons());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
