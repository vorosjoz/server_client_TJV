package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.repository;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Army;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArmyRepository extends CrudRepository<Army, Integer> {

}
