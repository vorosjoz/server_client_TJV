package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.controller.CustomException;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Army;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Army;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.repository.ArmyRepository;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.repository.ArmyRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
@Component
public class ArmyService {

    private ArmyRepository repository;


    public ArmyService(ArmyRepository repository) {
        this.repository = repository;
    }

    public Optional<Army> readById(Integer id) {
        return repository.findById(id);
    }

    public Army createEntity(Army entity) {
        return repository.save(entity);
    }

    public void deleteEntity(Integer id) {
        if (readById(id).isEmpty()) {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
        repository.deleteById(id);
    }

    public Collection<Army> readAll() {
        return (Collection<Army>) repository.findAll();
    }


    public Army updateEntity(Integer id, Army updatedArmy) throws CustomException {
        Optional<Army> optionalExistingArmy = repository.findById(id);
        if (optionalExistingArmy.isPresent()) {
            Army existingArmy = optionalExistingArmy.get();
            existingArmy.setName(updatedArmy.getName());
            return repository.save(existingArmy);
        } else {
            throw new CustomException("Army not found", org.springframework.http.HttpStatus.NOT_FOUND);
        }
    }


}
