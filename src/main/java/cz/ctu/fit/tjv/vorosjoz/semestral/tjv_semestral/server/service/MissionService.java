package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.controller.CustomException;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Mission;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.repository.MissionRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@Component
public class MissionService {

    private MissionRepository repository;


    public MissionService(MissionRepository repository) {
        this.repository = repository;
    }

    public Optional<Mission> readById(Integer id) {
        return repository.findById(id);
    }

    public Mission createEntity(Mission entity) {
        return repository.save(entity);
    }

    public void deleteEntity(Integer id) {
        if (readById(id).isEmpty()) {
            throw new CustomException("Entity not found", HttpStatus.NOT_FOUND);
        }
        repository.deleteById(id);
    }

    public Collection<Mission> readAll() {
        return (Collection<Mission>) repository.findAll();
    }


    public Mission updateEntity(Integer id, Mission updatedMission) {
        Optional<Mission> optionalExistingMission = repository.findById(id);
        if (optionalExistingMission.isPresent()) {
            Mission existingMission = optionalExistingMission.get();
            existingMission.setCodename(updatedMission.getCodename());
            return repository.save(existingMission);
        } else {
            throw new EntityNotFoundException("Mission not found...");
        }
    }

}
