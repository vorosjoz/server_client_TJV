-- Remove conflicting tables
DROP TABLE IF EXISTS army CASCADE;
DROP TABLE IF EXISTS mission CASCADE;
DROP TABLE IF EXISTS platoon CASCADE;
DROP TABLE IF EXISTS mission_platoon CASCADE;
-- End of removing

CREATE TABLE army (
                        id_army SERIAL NOT NULL,
                        name_army VARCHAR(32)
);

ALTER TABLE army ADD CONSTRAINT pk_army PRIMARY KEY (id_army);

CREATE TABLE mission (
                         id_mission SERIAL NOT NULL,
                         codename VARCHAR(32) NOT NULL
);
ALTER TABLE mission ADD CONSTRAINT pk_mission PRIMARY KEY (id_mission);
ALTER TABLE mission ADD CONSTRAINT uc_mission_codename UNIQUE (codename);

CREATE TABLE platoon (
                         id_platoon SERIAL NOT NULL,
                         id_army INTEGER NOT NULL,
                         name_platoon VARCHAR(32) NOT NULL,
                         nickname VARCHAR(32)
);

ALTER TABLE platoon ADD CONSTRAINT pk_platoon PRIMARY KEY (id_platoon);

CREATE TABLE mission_platoon (
                                 id_mission INTEGER NOT NULL,
                                 id_platoon INTEGER NOT NULL
);
ALTER TABLE mission_platoon ADD CONSTRAINT pk_mission_platoon PRIMARY KEY (id_mission, id_platoon);

ALTER TABLE platoon ADD CONSTRAINT fk_platoon_army FOREIGN KEY (id_army) REFERENCES army (id_army) ON DELETE CASCADE;
ALTER TABLE mission_platoon ADD CONSTRAINT fk_mission_platoon_mission FOREIGN KEY (id_mission) REFERENCES mission (id_mission) ON DELETE CASCADE;
ALTER TABLE mission_platoon ADD CONSTRAINT fk_mission_platoon_platoon FOREIGN KEY (id_platoon) REFERENCES platoon (id_platoon) ON DELETE CASCADE;

insert into Army (id_army, name_army) values (1, 'Red Army');
insert into Army (id_army, name_army) values (2, 'Blue Army');
insert into Army (id_army, name_army) values (3, 'Green Army');
insert into Army (id_army, name_army) values (4, 'Stalins Ten');
insert into Army (id_army, name_army) values (5, '1st Army');
insert into Army (id_army, name_army) values (6, '21st Army');
insert into Army (id_army, name_army) values (7, 'Putins Ten');
insert into Army (id_army, name_army) values (8, 'Russian Mafia');
insert into Army (id_army, name_army) values (9, 'KGB');
insert into Army (id_army, name_army) values (10, 'Spetsnaz GRU');
insert into Army (id_army, name_army) values (11, 'Spetsnaz');
insert into Army (id_army, name_army) values (12, 'Spetsnaz 2');

INSERT INTO Platoon (id_platoon, id_army, name_platoon, nickname)
VALUES (1, 3, 'The Funky Bunch', 'not null'),
       (2, 5, 'The Walking Dead', 'Zombie Hunters'),
       (3, 2, 'The Beer Brigade', 'Tipsy Troopers'),
       (4, 8, 'The Snack Attack', 'Munching Marines'),
       (5, 9, 'The Swole Squad', 'Muscle Militia'),
       (6, 4, 'The Codebreakers', 'Cryptic Commandos'),
       (7, 1, 'The Spice Girls', 'Spicy Soldiers'),
       (8, 7, 'The Couch Potatoes', 'Lazy Legionnaires'),
       (9, 6, 'The Ninja Ninjas', 'fhshnfjkl'),
       (10, 10, 'The Dream Team', 'aaaa'),
       (11, 1, 'The Bookworms', 'Intellectual Infantry'),
       (12, 3, 'The Party Platoon', 'Fiesta Fighters'),
       (13, 2, 'The Monster Mash', 'asasas'),
       (14, 4, 'The Rock Stars', 'Headbanging Heroes'),
       (15, 6, 'The Avocado Army', 'Guac-tastic Troops'),
       (16, 5, 'The Meme Machines', 'Dank Defenders'),
       (17, 8, 'The Sleepwalkers', 'Somnolent Soldiers'),
       (18, 7, 'The Beanie Brigade', 'Hippies for Peace'),
       (19, 10, 'The Cold Blooded Killers', 'Lizardmen'),
       (20, 9, 'The Feather Squad', 'A bunch of chickens');

INSERT INTO Mission (id_mission, codename) VALUES (1, 'Project Shadowgate');
INSERT INTO Mission (id_mission, codename) VALUES (2, 'Operation Quick Action');
INSERT INTO Mission (id_mission, codename) VALUES (3, 'Keugelblitz');
INSERT INTO Mission (id_mission, codename) VALUES (4, 'Blitzkrieg nach Polen');
INSERT INTO Mission (id_mission, codename) VALUES (5, 'Matrix');
INSERT INTO Mission (id_mission, codename) VALUES (6, 'Zauberer');
INSERT INTO Mission (id_mission, codename) VALUES (7, 'Special Military Operation');
INSERT INTO Mission (id_mission, codename) VALUES (8, 'Mission To The Center of Earth');
INSERT INTO Mission (id_mission, codename) VALUES (9, 'Moony Loony Tunes');
INSERT INTO Mission (id_mission, codename) VALUES (10, 'Manhattan Project Russian Style');
INSERT INTO Mission (id_mission, codename) VALUES (11, 'Kto mi zezral poslednu palacinku');


INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (2, 8);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (7, 3);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (9, 12);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (1, 17);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (10, 2);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (6, 5);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (3, 18);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (8, 7);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (4, 11);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (4, 20);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (5, 20);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (5, 8);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (5, 1);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (1, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (2, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (3, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (4, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (5, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (6, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (7, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (8, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (9, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (10, 19);
INSERT INTO Mission_Platoon (id_mission, id_platoon) VALUES (11, 19);