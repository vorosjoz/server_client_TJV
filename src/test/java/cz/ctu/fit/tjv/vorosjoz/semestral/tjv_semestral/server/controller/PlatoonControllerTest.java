package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.controller;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Platoon;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.model.Mission;
import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service.PlatoonService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.List;
@WebMvcTest(PlatoonController.class)
class PlatoonControllerTest {
    @MockBean
    private PlatoonController platoonController;
    @Test
    void testCRUD() {
        Platoon platoon = new Platoon();
        platoon.setId(0);
        platoon.setName("testPlatoon");
        Mission mission1 = new Mission();
        mission1.setId(0);
        mission1.setCodename("testMission1");
        Mission mission2 = new Mission();
        mission2.setId(1);
        mission2.setCodename("testMission2");
        platoon.setMissions(List.of(new Mission[]{mission1, mission2}));
        //create
        OngoingStubbing<ResponseEntity<Platoon>> responseEntityOngoingStubbing = Mockito
                .when(platoonController.create(platoon)).thenReturn(ResponseEntity.ok(platoon));
        //read
        Mockito.when(platoonController.read(0)).thenReturn(platoon);
        //edit
        Platoon newPlatoon = new Platoon();
        newPlatoon.setId(0);
        newPlatoon.setName("newTestPlatoon");
        newPlatoon.setMissions(List.of(new Mission[]{mission1}));
        OngoingStubbing<ResponseEntity<Platoon>> responseEntityOngoingStubbing1 = Mockito
                .when(platoonController.update(0, newPlatoon)).thenReturn(ResponseEntity.ok(newPlatoon));
        //delete
        OngoingStubbing<ResponseEntity<Void>> responseEntityOngoingStubbing2 = Mockito
                .when(platoonController.delete(0)).thenReturn(ResponseEntity.noContent().build());

    }

}