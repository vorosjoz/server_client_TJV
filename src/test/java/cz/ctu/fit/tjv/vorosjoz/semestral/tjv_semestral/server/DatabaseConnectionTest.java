package cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server;

import cz.ctu.fit.tjv.vorosjoz.semestral.tjv_semestral.server.service.ArmyService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnectionTest {
    @Test
    public static void main(String[] args) {
        String jdbcUrl = "jdbc:postgresql://localhost:5432/postgres";
        String username = "postgres";
        String password = "tjv";

        try (Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
            System.out.println("Connection successful!");
        } catch (SQLException e) {
            System.err.println("Connection failed. Error: " + e.getMessage());
        }
    }
}
