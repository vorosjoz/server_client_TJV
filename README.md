# Army database
The program contains 3 entities. `Army`, `Mission` and `Platoon`. An army consists of many platoons, a platoon can co on many missions and a mission can be attended by many platoons. The program allows storing, creating and deleting any of these entities.

## Entity relation schema
![Schema](tjv_semestral_schema.png)

